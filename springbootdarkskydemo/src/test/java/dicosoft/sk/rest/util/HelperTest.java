package dicosoft.sk.rest.util;

import dicosoft.sk.rest.model.Location;
import org.junit.Assert;
import org.junit.Test;
import tk.plogitech.darksky.forecast.model.DailyDataPoint;


public class HelperTest {

    @Test
    public void evalueLocationCriteria() {

        Location locationJASTARNIA = new Location();
        locationJASTARNIA.setWindSpeed(9.9); //Current for the day
        locationJASTARNIA.setWeather(6.96);  //Current for the day
        locationJASTARNIA.setType(TypeLocationsWindsurfing.JASTARNIA);
        DailyDataPoint dailyDataPointailyDataPointJASTARNIA = new DailyDataPoint();
        locationJASTARNIA.setDailyDataPoint(dailyDataPointailyDataPointJASTARNIA);
        dailyDataPointailyDataPointJASTARNIA.setWindSpeed(9.19);
        dailyDataPointailyDataPointJASTARNIA.setTemperatureHigh(7.71);
        dailyDataPointailyDataPointJASTARNIA.setTemperatureLow(3.45);

        Location locationWLADYSLAWOWO = new Location();
        locationWLADYSLAWOWO.setWindSpeed(10.78); //Current for the day
        locationWLADYSLAWOWO.setWeather(7.05);    //Current for the day
        locationWLADYSLAWOWO.setType(TypeLocationsWindsurfing.WLADYSLAWOWO);
        DailyDataPoint dailyDataPointailyDataPointWLADYSLAWOWO = new DailyDataPoint();
        locationWLADYSLAWOWO.setDailyDataPoint(dailyDataPointailyDataPointWLADYSLAWOWO);
        dailyDataPointailyDataPointWLADYSLAWOWO.setWindSpeed(9.63);
        dailyDataPointailyDataPointWLADYSLAWOWO.setTemperatureHigh(7.95);
        dailyDataPointailyDataPointWLADYSLAWOWO.setTemperatureLow(2.98);

        Location bestLocation = SelectionCriteriaHelper.getBestLocation(locationJASTARNIA, locationWLADYSLAWOWO);

        Assert.assertTrue("Formula", 0 < bestLocation.getValueCalculatedFormula());

        Assert.assertNotNull(bestLocation);


        locationWLADYSLAWOWO.setWindSpeed(19); //Current for the day
        bestLocation = SelectionCriteriaHelper.getBestLocation(locationJASTARNIA, locationWLADYSLAWOWO);

        Assert.assertEquals(TypeLocationsWindsurfing.JASTARNIA, bestLocation.getType());


        locationJASTARNIA.setWeather(36); //Current for the day
        bestLocation = SelectionCriteriaHelper.getBestLocation(locationJASTARNIA, locationWLADYSLAWOWO);

        Assert.assertNull("No location", bestLocation);


        locationWLADYSLAWOWO.setWindSpeed(10.78); //Current for the day
        bestLocation = SelectionCriteriaHelper.getBestLocation(locationJASTARNIA, locationWLADYSLAWOWO);

        Assert.assertEquals(TypeLocationsWindsurfing.WLADYSLAWOWO, bestLocation.getType());

    }


    @Test(expected = NullPointerException.class)
    public void checkParametersLocation() {
        SelectionCriteriaHelper.getBestLocation(null, null);
    }
}