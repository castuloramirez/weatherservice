package dicosoft.sk.rest.util;

public class StringHelper {


    /**
     * @param windSpeed
     * @param temperature
     * @return
     */
    public static boolean evaluateRange(double windSpeed, double temperature) {
        //If the wind speed is not within <5; 18> (m/s) and the temperature is not in the range <5; 35> (°C),
        if (!(5 <= windSpeed && windSpeed <= 18)) {
            return false;
        }
        if (!(5 <= temperature && temperature <= 35)) {
            return false;
        }
        return true;
    }

    public static double formula(double windSpeed, double temperatureLow, double temperatureHigh) {
        double value = windSpeed * 3 + (temperatureLow + temperatureHigh) / 2;
        return value;
    }

}