package dicosoft.sk.rest.util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class DateHelper {

    /**
     * @param instant
     * @return
     */
    public static String getString(Instant instant) {
        String formattedDate = null;
        try {
            Date myDate = Date.from(instant);
            SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy HH:mm:ss");
            formattedDate = formatter.format(myDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * @param dateInString
     * @return
     */
    public static Instant getInstant(String dateInString) {
        Instant instant = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(dateInString);
            instant = date.toInstant();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instant;
    }
}
