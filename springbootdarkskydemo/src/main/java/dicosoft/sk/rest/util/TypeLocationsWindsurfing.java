package dicosoft.sk.rest.util;

public enum TypeLocationsWindsurfing {

    // enum fields
    JASTARNIA(1,"Jastarnia, Poland"),
    WLADYSLAWOWO(2, "Wladyslawowo, Poland"),
    RHODES_GREECE(3,"Rhodes, Greece"),
    COLUMBIA_RIVER_WASHINGTON(4, "Columbia River, Washington USA"),
    COLUMBIA_RIVER_OREGON(5, "Columbia River, Oregon USA"),
    VIRGIN_ISLANDS_US(6,"U.S. Virgin Islands"),
    VIRGIN_ISLANDS_UK(7, "British Virgin Islands"),
    DOMINICAN_REPUBLIC(8, "Dominican Republic, country in the Caribbean"),
    PHILIPPINES_BORACAY_MALAY(9, "Boracay, island in Malay, Aklan, Philippines"),
    CARIBBEAN_BONAIRE(10, "Bonaire, Netherlands Overseas region Caribbean Netherlands"),
    OUTER_BANKS_USA(11,"Outer Banks Island in North Carolina, USA"),
    EGYPT_RED_SEA(12,"Egypt's Red Sea"),
    SPAIN_FUERTEVENTURA(13, "Fuerteventura, Spain’s Canary Islands"),
    HAWAII_KIHEI_MAUI(14,"Kihei,in Maui County, Hawaii, USA");

    // internal state
    private int priority;

    public String getPlaceName() {
        return placeName;
    }

    private String placeName;

    // constructor
    TypeLocationsWindsurfing(final int t, final String n) {
        this.priority = t;
        this.placeName = n;
    }

    public int getPriority() {
        return priority;
    }
}