package dicosoft.sk.rest.util;

import dicosoft.sk.rest.model.Location;

public class SelectionCriteriaHelper {

    /**
     * @param locationJASTARNIA
     * @param locationWLADYSLAWOWO
     * @return
     */
    public static Location getBestLocation(Location locationJASTARNIA, Location locationWLADYSLAWOWO) {

        resetValues(locationJASTARNIA);
        resetValues(locationWLADYSLAWOWO);

        boolean evaluateJ = StringHelper.evaluateRange(locationJASTARNIA.getWindSpeed(), locationJASTARNIA.getWeather());
        locationJASTARNIA.setSurfingConditionsOkay(evaluateJ);

        boolean evaluateW = StringHelper.evaluateRange(locationWLADYSLAWOWO.getWindSpeed(), locationWLADYSLAWOWO.getWeather());
        locationWLADYSLAWOWO.setSurfingConditionsOkay(evaluateW);

        if ((locationJASTARNIA.isSurfingConditionsOkay()) && (locationWLADYSLAWOWO.isSurfingConditionsOkay())) {
            locationJASTARNIA.setValueCalculatedFormula(StringHelper.formula(locationJASTARNIA.getDailyDataPoint().getWindSpeed(),
                    locationJASTARNIA.getDailyDataPoint().getTemperatureLow(),
                    locationJASTARNIA.getDailyDataPoint().getTemperatureHigh()));

            locationWLADYSLAWOWO.setValueCalculatedFormula(StringHelper.formula(locationWLADYSLAWOWO.getDailyDataPoint().getWindSpeed(),
                    locationWLADYSLAWOWO.getDailyDataPoint().getTemperatureLow(), locationWLADYSLAWOWO.getDailyDataPoint().getTemperatureHigh()));


            if (locationWLADYSLAWOWO.getValueCalculatedFormula() > locationJASTARNIA.getValueCalculatedFormula()) {
                return locationWLADYSLAWOWO;

            } else {
                return locationJASTARNIA;
            }
        } else if (locationJASTARNIA.isSurfingConditionsOkay()) {
            return locationJASTARNIA;
        } else if (locationWLADYSLAWOWO.isSurfingConditionsOkay()) {
            return locationWLADYSLAWOWO;
        }
        return null;
    }

    public static void resetValues(Location location) {
        location.setSurfingConditionsOkay(false);
        location.setValueCalculatedFormula(0.0);
    }
}