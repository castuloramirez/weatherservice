package dicosoft.sk.rest.controller;


import dicosoft.sk.rest.dao.LocationWeatherServiceDAO;
import dicosoft.sk.rest.model.Location;
import dicosoft.sk.rest.model.Locations;
import dicosoft.sk.rest.model.SearchWeatherServiceLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/weatherservice")
public class LocationWeatherServiceController {
    @Autowired
    private LocationWeatherServiceDAO locationWeatherServiceDAO;

    @PostMapping(path = "/best", consumes = "application/json", produces = "application/json")
    public Location getLocationByDate(@RequestBody SearchWeatherServiceLocation searchWeatherServiceLocation) {
        System.out.println("+++++++++++++++++" + searchWeatherServiceLocation.toString());
        return locationWeatherServiceDAO.getLocationsByDate(searchWeatherServiceLocation);
    }

    @GetMapping(path = "/search", produces = "application/json")
    public Locations getLocations() {
        System.out.println("+++++++++++++++++++++");
        return locationWeatherServiceDAO.getAllLocations();
    }
}