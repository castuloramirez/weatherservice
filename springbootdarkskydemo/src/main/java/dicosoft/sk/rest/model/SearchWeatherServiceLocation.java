package dicosoft.sk.rest.model;

public class SearchWeatherServiceLocation {

    private String date;//yyyy-mm-dd date format


    public SearchWeatherServiceLocation(String date) {
        this.date = date;
    }

    public SearchWeatherServiceLocation() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "SearchWeatherServiceLocation{" +
                "date='" + date + '\'' +
                '}';
    }
}
