package dicosoft.sk.rest.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import dicosoft.sk.rest.util.TypeLocationsWindsurfing;
import tk.plogitech.darksky.forecast.model.DailyDataPoint;

public class Location {

    private Integer id;
    private String name;
    private double lat;
    private double lng;
    @JsonIgnore
    private TypeLocationsWindsurfing type;
    @JsonIgnore
    private boolean isSurfingConditionsOkay = false;

    @JsonIgnore
    private double valueCalculatedFormula=0.0;

    @JsonProperty("current temperature")
    private double weather;

    @JsonProperty("current wind speed")
    private double windSpeed;

    @JsonProperty("daily")
    private DailyDataPoint dailyDataPoint;

    public Location() {
    }

    public Location(Integer id, String name, double lat, double lng, TypeLocationsWindsurfing type) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.type = type;
    }

    public double getWeather() {
        return weather;
    }

    public void setWeather(double weather) {
        this.weather = weather;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public DailyDataPoint getDailyDataPoint() {
        return dailyDataPoint;
    }

    public void setDailyDataPoint(DailyDataPoint dailyDataPoint) {
        this.dailyDataPoint = dailyDataPoint;
    }

    public TypeLocationsWindsurfing getType() {
        return type;
    }

    public void setType(TypeLocationsWindsurfing type) {
        this.type = type;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSurfingConditionsOkay() {
        return isSurfingConditionsOkay;
    }

    public void setSurfingConditionsOkay(boolean surfingConditionsOkay) {
        isSurfingConditionsOkay = surfingConditionsOkay;
    }

    public double getValueCalculatedFormula() {
        return valueCalculatedFormula;
    }

    public void setValueCalculatedFormula(double valueCalculatedFormula) {
        this.valueCalculatedFormula = valueCalculatedFormula;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", type='" + type.name() + '\'' +
                '}';
    }
}
