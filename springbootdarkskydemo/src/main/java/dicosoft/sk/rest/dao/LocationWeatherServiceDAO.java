package dicosoft.sk.rest.dao;


import dicosoft.sk.rest.model.Location;
import dicosoft.sk.rest.model.Locations;
import dicosoft.sk.rest.model.SearchWeatherServiceLocation;
import dicosoft.sk.rest.util.*;
import org.springframework.stereotype.Repository;
import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.*;
import tk.plogitech.darksky.forecast.model.DailyDataPoint;
import tk.plogitech.darksky.forecast.model.Forecast;
import tk.plogitech.darksky.forecast.model.Latitude;
import tk.plogitech.darksky.forecast.model.Longitude;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;

@Repository
public class LocationWeatherServiceDAO {

    private static Locations list = new Locations();

    /**
     * embedded list of windsurfing locations (including geographical coordinates)
     */
    static {
        TypeLocationsWindsurfing typeLocationsWindsurfing = TypeLocationsWindsurfing.JASTARNIA;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 54.6957333, 18.678839, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.WLADYSLAWOWO;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 54.79074, 18.403, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.RHODES_GREECE;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 36.44597, 28.217241, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.COLUMBIA_RIVER_WASHINGTON;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 46.2733, -124.0720, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.COLUMBIA_RIVER_OREGON;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 46.2017, -123.9450, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.VIRGIN_ISLANDS_US;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 18.319410, -64.703247, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.VIRGIN_ISLANDS_UK;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 18.436539, -64.618103, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.DOMINICAN_REPUBLIC;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 19.75943, -70.417941, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.PHILIPPINES_BORACAY_MALAY;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 11.967375, 121.924812, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.CARIBBEAN_BONAIRE;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 12.1499996, -68.2666702, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.OUTER_BANKS_USA;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 35.5584932, -75.46651580000002, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.EGYPT_RED_SEA;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 24.846565, 35.507812, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.SPAIN_FUERTEVENTURA;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 28.3333300, -14.0200000, typeLocationsWindsurfing));
        typeLocationsWindsurfing = TypeLocationsWindsurfing.HAWAII_KIHEI_MAUI;
        list.getLocationList().add(new Location(typeLocationsWindsurfing.getPriority(), typeLocationsWindsurfing.getPlaceName(), 20.693247, -156.431078, typeLocationsWindsurfing));

        if (Constants.PROXY) {
            System.setProperty("http.proxyHost", Constants.PROXY_HOST);
            System.setProperty("http.proxyPort", Constants.PROXY_PORT);
            System.setProperty("https.proxyHost", Constants.PROXY_HOST);
            System.setProperty("https.proxyPort", Constants.PROXY_PORT);
        }
    }

    /**
     * @param sl
     * @return
     */
    public Location getLocationsByDate(SearchWeatherServiceLocation sl) {

        try {
            String stringDateParameter = sl.getDate();
            //Geting the first two : JASTARNIA and  WLADYSLAWOWO
            Location locationJastarnia = list.getLocationList().get(0);
            this.getData(stringDateParameter, locationJastarnia);
            Location locationWladysawowo = list.getLocationList().get(1);
            this.getData(stringDateParameter, locationWladysawowo);
            Location bestLocation = SelectionCriteriaHelper.getBestLocation(locationJastarnia, locationWladysawowo);
            //location = locationWladysawowo;
            return bestLocation;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Full Search of the Locations.
     *
     * @return
     */
    public Locations getAllLocations() {
        List<Location> listOrder = list.getLocationList();
        listOrder.forEach(location -> {
            try {
                getData(null, location);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        listOrder.sort(Comparator.comparing(a -> a.getType().getPriority()));
        list.setLocationList(listOrder);
        return list;
    }

    private void getData(String stringDateParameter, Location location) throws Exception {

        Longitude longitude = new Longitude(location.getLng());
        Latitude latitude = new Latitude(location.getLat());
        Instant time;
        if (stringDateParameter != null) {
            time = DateHelper.getInstant(stringDateParameter);
        } else {
            time = Instant.now();
        }
      //  System.out.println("The Test Instant:" + DateHelper.getString(time));

        ForecastRequest request = new ForecastRequestBuilder()
                .key(new APIKey(Constants.apiKey))
                .time(time)
                .language(ForecastRequestBuilder.Language.en)
                .units(ForecastRequestBuilder.Units.si) //Celsius
                .location(new GeoCoordinates(longitude, latitude)).build();

  /*    DarkSkyClient client = new DarkSkyClient();
        String forecast = client.forecastJsonString(request);
        System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::Forecast DATA -----------------------------------------------------------> " + forecast);
*/
        DarkSkyJacksonClient clientJack = new DarkSkyJacksonClient();
        Forecast forecastJack = clientJack.forecast(request);
        List<DailyDataPoint> dailydata = forecastJack.getDaily().getData();
        location.setWeather(forecastJack.getCurrently().getTemperature());// degrees Celsius
        location.setWindSpeed(forecastJack.getCurrently().getWindSpeed());
        if ((dailydata != null) && (dailydata.size() > 0)) {
            DailyDataPoint dailyDataPoint = dailydata.get(0);
            location.setDailyDataPoint(dailyDataPoint);
        }
    }

}