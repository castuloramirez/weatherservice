# Spring Boot Dark Sky Demo V1.0

Tools : IntelliJ Idea 2019.3 Community, Postman

Technologies : Java 8, Spring Boot

1)	Running the app

springbootdarkskydemo> mvn clean install -U -Dmaven.test.skip=true


springbootdarkskydemo> mvn spring-boot:run


2)	http://localhost:8080/weatherservice/best   (POST)

{
"date": "2020-03-29"
}
 
3) http://localhost:8080/weatherservice/search  (GET)


4) Set to True if you are under Proxy and change the Constants.PROXY_HOST

if (Constants.PROXY) {
    System.setProperty("http.proxyHost", Constants.PROXY_HOST);
    System.setProperty("http.proxyPort", Constants.PROXY_PORT);
    System.setProperty("https.proxyHost", Constants.PROXY_HOST);
    System.setProperty("https.proxyPort", Constants.PROXY_PORT);
}


5) Please don't use :

The problems with Swagger

https://www.novatec-gmbh.de/en/blog/the-problems-with-swagger/

https://dzone.com/articles/swagger-great

6) Best practices for rest paths 

https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9






